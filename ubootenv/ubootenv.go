// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

// Package ubootenv implements access to U-Boot environment compatible with
// the native C utilities and their configuration files.
package ubootenv

import (
	"bufio"
	"bytes"
	"encoding/binary"
	"errors"
	"fmt"
	"hash/crc32"
	"io"
	"os"
	"strconv"
	"strings"
)

// ConfigFile is the name of the U-Boot environment configuration file.
type ConfigFile string

var errNotImplemented = errors.New("not implemented")

// Location pairs file name with a line number.
type Location struct {
	FileName string
	LineNo   int
}

// String returns textual representation of the file name and line number.
func (l Location) String() string {
	return fmt.Sprintf("%s:%d", l.FileName, l.LineNo)
}

// ParseError describes a parser error.
type ParseError struct {
	Location Location
	Err      error
}

// Error combines location and the wrapped error.
func (e *ParseError) Error() string {
	return fmt.Sprintf("%s: %s", e.Location, e.Err)
}

// Unwrap returns the wrapped error.
func (e *ParseError) Unwrap() error {
	return e.Err
}

// LoadConfig loads U-Boot environment configuration file.
func (cf ConfigFile) LoadConfig() (*Config, error) {
	return LoadConfig(string(cf))
}

// LoadConfig loads the given U-Boot environment configuration file.
func LoadConfig(fileName string) (*Config, error) {
	f, err := os.Open(fileName)
	if err != nil {
		return nil, err
	}

	defer func() {
		_ = f.Close()
	}()

	return ReadConfig(f, f.Name())
}

var (
	errInsufficientDevices = errors.New("at least one device is required")
	errTooManyDevices      = errors.New("at most two devices are allowed")
	errEnvSizeMismatch     = errors.New("primary and redundant device must use the same environment size")
)

// ReadConfig reads U-Boot environment configuration from the given reader.
//
// The fileName argument is only used for error reporting.
func ReadConfig(r io.Reader, fileName string) (*Config, error) {
	scanner := bufio.NewScanner(r)

	loc := Location{FileName: fileName}

	var c Config

	var devCount int

	for loc.LineNo = 1; scanner.Scan(); loc.LineNo++ {
		rawLine := scanner.Bytes()
		if bytes.IndexRune(rawLine, '#') == 0 {
			continue
		}

		if len(bytes.TrimSpace(rawLine)) == 0 {
			continue
		}

		var err error

		switch devCount {
		case 0:
			err = c.Device.UnmarshalText(rawLine)
		case 1:
			c.RedundantDevice = new(Device)
			err = c.RedundantDevice.UnmarshalText(rawLine)
		default:
			err = errTooManyDevices
		}

		if err != nil {
			return nil, &ParseError{Location: loc, Err: err}
		}

		if c.RedundantDevice != nil && c.Device.EnvSize != c.RedundantDevice.EnvSize {
			return nil, &ParseError{Location: loc, Err: errEnvSizeMismatch}
		}

		devCount++
	}

	if err := scanner.Err(); err != nil {
		return nil, &ParseError{Location: loc, Err: err}
	}

	if devCount == 0 {
		return nil, &ParseError{Location: loc, Err: errInsufficientDevices}
	}

	return &c, nil
}

// Config describes storage of U-Boot environment variables.
type Config struct {
	Device Device
	// U-Boot uses two schemes for storing environment variables, with or
	// without redundant copy. Redundant copy is used with memory technology
	// devices(MTD). Using redundancy reserves one byte of data.
	// Interpretation of this byte depends on the the type of MTD technology.
	// Some device types use active/inactive flag, others use a counter.
	RedundantDevice *Device
}

var (
	errBadCRC = errors.New("bad CRC")
)

const plainHeaderSize = 4

// LoadEnv loads environment variables from storage.
//
// CRC data is verified, invalid data is never returned.
//
// Redundant storage device is not supported yet.
func (cfg *Config) LoadVars() (*Variables, error) {
	if cfg.RedundantDevice != nil {
		panic(errNotImplemented)
	}

	data, err := cfg.Device.LoadData()
	if err != nil {
		return nil, err
	}

	crcOk := crc32.ChecksumIEEE(data[plainHeaderSize:]) == binary.LittleEndian.Uint32(data[:plainHeaderSize])
	if !crcOk {
		return nil, errBadCRC
	}

	return &Variables{Data: data, HeaderSize: plainHeaderSize}, nil
}

// NewVars returns empty variables.
func (cfg *Config) NewVars() *Variables {
	return &Variables{Data: make([]byte, cfg.Device.EnvSize), HeaderSize: plainHeaderSize}
}

// SaveEnv saves environment variables to storage.
//
// CRC present in the header is re-calcaulated.
func (cfg *Config) SaveVars(vars *Variables) error {
	if cfg.RedundantDevice != nil {
		panic(errNotImplemented)
	}

	newCrc := crc32.ChecksumIEEE(vars.Data[vars.HeaderSize:])
	binary.LittleEndian.PutUint32(vars.Data[:vars.HeaderSize], newCrc)

	return cfg.Device.SaveData(vars.Data)
}

// Device describes storage for environment variables.
type Device struct {
	DeviceName   string // Path to a regular file, block device or a MTD character device.
	DeviceOffset int    // Negative values represent offset from the end of the device.
	EnvSize      int
	SectorSize   int
	SectorCount  int
}

var (
	errNotRegularFile  = errors.New("only regular files are supported")
	errEnvDataMismatch = errors.New("data size is different from environment size")
)

// LoadData loads the data from the device.
//
// Data is loaded from the right offset, using the means of access determined
// at runtime, based on the type of the file (regular file, block device or
// MTD character device).
//
// At present only regular devices are supported.
func (d *Device) LoadData() ([]byte, error) {
	f, err := os.Open(d.DeviceName)
	if err != nil {
		return nil, err
	}

	defer func() {
		_ = f.Close()
	}()

	fi, err := f.Stat()
	if err != nil {
		return nil, err
	}

	if !fi.Mode().IsRegular() {
		// TODO: add support for block devices and MTD
		return nil, errNotRegularFile
	}

	off := int64(d.DeviceOffset)
	if off < 0 {
		off = fi.Size() + off
	}

	data := make([]byte, d.EnvSize)

	if _, err := f.ReadAt(data, off); err != nil {
		return nil, err
	}

	return data, nil
}

func (d *Device) SaveData(data []byte) error {
	if len(data) != d.EnvSize {
		return errEnvDataMismatch
	}

	f, err := os.OpenFile(d.DeviceName, os.O_RDWR|os.O_CREATE, 0o600)
	if err != nil {
		return err
	}

	defer func() {
		_ = f.Close()
	}()

	fi, err := f.Stat()
	if err != nil {
		return err
	}

	if !fi.Mode().IsRegular() {
		// TODO: add support for block devices and MTD
		return errNotRegularFile
	}

	off := int64(d.DeviceOffset)
	if off < 0 {
		off = fi.Size() + off
	}

	if _, err := f.WriteAt(data, off); err != nil {
		return err
	}

	return nil
}

var (
	errNegativeSectorCount = errors.New("sector count cannot be negative")
	errNegativeSectorSize  = errors.New("sector size cannot be negative")
	errNonPositiveEnvSize  = errors.New("environment size must be greater than zero")
	errExpectedFields      = errors.New("expected device name, offset and environment size")
	errUnexpectedFields    = errors.New("expected device name, offset and environment size, sector size and sector count")
)

// UnmarshalText parses device description compatible with /etc/fw_env.config.
//
// The description is a line of text with space-separated fields. The fields
// are laid out in the same order as they are defined in the Device
// structure. SectorCount is optional and defaults to one. SectorSize is also
// optional and defaults to EnvSize. DeviceOffset can be negative, to imply
// offset from the end of the device. DeviceName is any path and is not
// verified further.
func (d *Device) UnmarshalText(text []byte) error {
	flds := strings.Fields(string(text))

	switch len(flds) {
	case 5:
		// Parse number of sectors used by the environment.
		n, err := strconv.ParseInt(flds[4], 0, 32)
		if err != nil {
			return err
		}

		if n < 0 {
			return errNegativeSectorCount
		}

		d.SectorCount = int(n)

		fallthrough
	case 4:
		n, err := strconv.ParseInt(flds[3], 0, 32)
		if err != nil {
			return err
		}

		if n < 0 {
			return errNegativeSectorSize
		}

		d.SectorSize = int(n)

		fallthrough
	case 3:
		// Parse size of the environment data.
		n, err := strconv.ParseInt(flds[2], 0, 32)
		if err != nil {
			return err
		}

		if n <= 0 {
			return errNonPositiveEnvSize
		}

		d.EnvSize = int(n)

		// Parse offset of environment data into the device.
		n, err = strconv.ParseInt(flds[1], 0, 32)
		if err != nil {
			return err
		}
		// Device offset can be negative, no check is necessary.

		d.DeviceOffset = int(n)

		// Parse the device name.
		d.DeviceName = flds[0]

		// If sector size was missing or specified as zero, set it to the environment size.
		if d.SectorSize == 0 {
			d.SectorSize = d.EnvSize
		}

		// If sector count was missing or specified as zero, supply the implicit one.
		if d.SectorCount == 0 {
			d.SectorCount = 1
		}

		return nil
	case 0, 1, 2:
		return errExpectedFields
	default:
		return errUnexpectedFields
	}
}

// MarshalText crates device description compatible with /etc/fw_env.config.
func (d *Device) MarshalText() ([]byte, error) {
	var b bytes.Buffer

	// Writes to bytes.Buffer can only painc on failure.
	_, _ = fmt.Fprintf(&b, "%s %#x %#x", d.DeviceName, d.DeviceOffset, d.EnvSize)

	showSectorSize := d.SectorSize != d.EnvSize
	showSectorCount := d.SectorCount != 0 && d.SectorCount != 1

	if showSectorSize || showSectorCount {
		_, _ = fmt.Fprintf(&b, " %#x", d.SectorSize)
	}

	if showSectorCount {
		_, _ = fmt.Fprintf(&b, " %d", d.SectorCount)
	}

	return b.Bytes(), nil
}

// Variables contains U-boot environment variables.
type Variables struct {
	Data       []byte
	HeaderSize int
}

// Get returns the value of the given variable.
//
// If the variable is defined multiple times, the first value is returned. If
// the variable is not defined, an empty string is returned.
func (vars Variables) Get(key string) (value string) {
	v, _ := vars.Lookup(key)
	return v
}

// Lookup returns the value of the given variable.
//
// Lookup is like Get except that it also returns a boolean flag indicating if
// the variable was defined or not.
func (vars Variables) Lookup(key string) (value string, ok bool) {
	cur := NewCursor(vars.Data[vars.HeaderSize:])

	for cur.Next() {
		if cur.Key() == key {
			return cur.Value(), true
		}
	}

	return "", false
}

// Set sets the value of a given variable.
//
// Existing variable is resized in place, without affecting the order. If the
// variable is not defined it is inserted after all the existing variables.
//
// Set may fail if the value is too long to store. In that case the
// environment block is not modified.
//
// If there are multiple variables with the same name, they are all modified.
func (vars *Variables) Set(key, value string) error {
	cur := NewCursor(vars.Data[vars.HeaderSize:])
	found := false

	for cur.Next() {
		if cur.Key() == key {
			found = true

			if err := cur.SetValue(value); err != nil {
				return err
			}
		}
	}

	if !found {
		return cur.InsertBefore(key, value)
	}

	return nil
}

// Delete deletes the given variable.
//
// If there are multiple variables with the same name, they are all deleted.
func (vars *Variables) Delete(key string) {
	cur := NewCursor(vars.Data[vars.HeaderSize:])

	for cur.Next() {
		if cur.Key() == key {
			cur.Delete()
		}
	}
}
