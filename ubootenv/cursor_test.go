// SPDX-License-Identifier: Apache-2.0
// SPDX-FileCopyrightText: Huawei Inc.

package ubootenv_test

import (
	"bytes"
	"errors"
	"testing"

	"gitlab.com/zyga-aka-zygoon/go-u-boot/ubootenv"
)

func TestCursorEmptyBuf(t *testing.T) {
	cur := ubootenv.NewCursor([]byte{})

	if cur.Next() != false {
		t.Fatalf("Expected to find no entries")
	}
}

func TestCursorSeesValidEntry(t *testing.T) {
	cur := ubootenv.NewCursor([]byte("key=value\x00"))

	if cur.Next() != true {
		t.Fatalf("Expected to find the first entry")
	}

	if e := cur.Entry(); e != "key=value\x00" {
		t.Fatalf("Unexpected entry: %q", e)
	}

	if e := cur.Key(); e != "key" {
		t.Fatalf("Unexpected entry key: %q", e)
	}

	if e := cur.Value(); e != "value" {
		t.Fatalf("Unexpected entry value: %q", e)
	}

	if cur.Next() != false {
		t.Fatalf("Expected to find only one entry")
	}
}

func TestCursorSeesUnterminatedRecord(t *testing.T) {
	cur := ubootenv.NewCursor([]byte("key=value"))

	if cur.Next() != true {
		t.Fatalf("Expected to find the first entry")
	}

	if e := cur.Entry(); e != "key=value" {
		t.Fatalf("Unexpected entry: %q", e)
	}

	if e := cur.Key(); e != "key" {
		t.Fatalf("Unexpected entry key: %q", e)
	}

	if e := cur.Value(); e != "value" {
		t.Fatalf("Unexpected entry value: %q", e)
	}

	if cur.Next() != false {
		t.Fatalf("Expected to find only one entry")
	}
}

func TestCursorAdvancesThroughEntries(t *testing.T) {
	cur := ubootenv.NewCursor([]byte("k1=v1\x00k2=v2\x00"))

	if cur.Next() != true {
		t.Fatalf("Expected to find the first entry")
	}

	if e := cur.Entry(); e != "k1=v1\x00" {
		t.Fatalf("Unexpected entry: %q", e)
	}

	if cur.Next() != true {
		t.Fatalf("Expected to find the second entry")
	}

	if e := cur.Entry(); e != "k2=v2\x00" {
		t.Fatalf("Unexpected entry: %q", e)
	}

	if cur.Next() != false {
		t.Fatalf("Expected to find only two entries")
	}
}

func TestCursorRecognizesUnusedSpace(t *testing.T) {
	cur := ubootenv.NewCursor([]byte("key=value\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00"))

	if cur.Next() != true {
		t.Fatalf("Expected to find the first entry")
	}

	if cur.Next() != false {
		t.Fatalf("Expected to find only one entry")
	}
}

func TestCursorSetValueSameSize(t *testing.T) {
	cur := ubootenv.NewCursor([]byte("key=1\x00\x00\x00\x00\x00\x00\x00"))

	if cur.Next() != true {
		t.Fatalf("Expected to find the first entry")
	}

	if ent := cur.Entry(); ent != "key=1\x00" {
		t.Fatalf("Unexpected entry before assignment: %q", ent)
	}

	if err := cur.SetValue("0"); err != nil {
		t.Fatalf("Cannot set new value: %v", err)
	}

	if ent := cur.Entry(); ent != "key=0\x00" {
		t.Fatalf("Unexpected entry after assignment: %q", ent)
	}

	if cur.Next() != false {
		t.Fatalf("Expected to find only one entry")
	}
}

func TestCursorSetValueSmallerSize(t *testing.T) {
	block := []byte("key=potato\x00after=pristine\x00\x00")
	cur := ubootenv.NewCursor(block)

	if cur.Next() != true {
		t.Fatalf("Expected to find the first entry")
	}

	if ent := cur.Entry(); ent != "key=potato\x00" {
		t.Fatalf("Unexpected entry before assignment: %q", ent)
	}

	if err := cur.SetValue("foo"); err != nil {
		t.Fatalf("Cannot set new value: %v", err)
	}

	if ent := cur.Entry(); ent != "key=foo\x00" {
		t.Fatalf("Unexpected entry after assignment: %q", ent)
	}

	if !bytes.Equal(block, []byte("key=foo\x00after=pristine\x00\x00\x00\x00\x00")) {
		t.Fatalf("Unexpected block after assignment: %q", string(block))
	}

	if cur.Next() != true {
		t.Fatalf("Expected to find the second entry")
	}

	if ent := cur.Entry(); ent != "after=pristine\x00" {
		t.Fatalf("Unexpected second entry: %q", ent)
	}

	if cur.Next() != false {
		t.Fatalf("Expected to find only two entries")
	}
}

func TestCursorSetValueLargerSize(t *testing.T) {
	block := []byte("key=foo\x00after=pristine\x00\x00\x00\x00\x00")
	cur := ubootenv.NewCursor(block)

	if cur.Next() != true {
		t.Fatalf("Expected to find the first entry")
	}

	if ent := cur.Entry(); ent != "key=foo\x00" {
		t.Fatalf("Unexpected entry before assignment: %q", ent)
	}

	if err := cur.SetValue("potato"); err != nil {
		t.Fatalf("Cannot set new value: %v", err)
	}

	if ent := cur.Entry(); ent != "key=potato\x00" {
		t.Fatalf("Unexpected entry after assignment: %q", ent)
	}

	if !bytes.Equal(block, []byte("key=potato\x00after=pristine\x00\x00")) {
		t.Fatalf("Unexpected block after assignment: %q", string(block))
	}

	if cur.Next() != true {
		t.Fatalf("Expected to find the second entry")
	}

	if ent := cur.Entry(); ent != "after=pristine\x00" {
		t.Fatalf("Unexpected second entry: %q", ent)
	}

	if cur.Next() != false {
		t.Fatalf("Expected to find only two entries")
	}
}

func TestCursorSetValueInsufficientSpace(t *testing.T) {
	block := []byte("key=123\x00other=value\x00\x00")
	cur := ubootenv.NewCursor(block)

	if cur.Next() != true {
		t.Fatalf("Expected to find the first entry")
	}

	if ent := cur.Entry(); ent != "key=123\x00" {
		t.Fatalf("Unexpected entry before assignment: %q", ent)
	}

	if err := cur.SetValue("12345"); !errors.Is(err, ubootenv.ErrInsufficientSpace) {
		t.Fatalf("Unexpected success in setting the new value: %v", err)
	}

	if !bytes.Equal(block, []byte("key=123\x00other=value\x00\x00")) {
		t.Fatalf("Unexpected block after failed assignment: %q", string(block))
	}

	if ent := cur.Entry(); ent != "key=123\x00" {
		t.Fatalf("Unexpected entry after failed assignment: %q", ent)
	}

	if cur.Next() != true {
		t.Fatalf("Expected to find the second entry entry")
	}

	if ent := cur.Entry(); ent != "other=value\x00" {
		t.Fatalf("Unexpected second entry after failed assignment: %q", ent)
	}

	if cur.Next() != false {
		t.Fatalf("Expected to find only one entry")
	}
}

func TestCursorSetValueCorruptedEntry(t *testing.T) {
	block := []byte("corrupted\x00\x00")
	cur := ubootenv.NewCursor(block)

	if cur.Next() != true {
		t.Fatalf("Expected to find the first entry")
	}

	if ent := cur.Entry(); ent != "corrupted\x00" {
		t.Fatalf("Unexpected entry before assignment: %q", ent)
	}

	if err := cur.SetValue("potato"); !errors.Is(err, ubootenv.ErrCorruptedEntry) {
		t.Fatalf("Unexpected success in setting the new value: %v", err)
	}

	if ent := cur.Entry(); ent != "corrupted\x00" {
		t.Fatalf("Unexpected entry after failed assignment: %q", ent)
	}

	if !bytes.Equal(block, []byte("corrupted\x00\x00")) {
		t.Fatalf("Unexpected block after failed assignment: %q", string(block))
	}

	if cur.Next() != false {
		t.Fatalf("Expected to find only one entry")
	}
}

func TestCursorSetValueInvalidCursor(t *testing.T) {
	block := []byte("\x00")
	cur := ubootenv.NewCursor(block)

	if cur.Next() != false {
		t.Fatalf("Expected to find no entries")
	}

	if err := cur.SetValue("foo"); !errors.Is(err, ubootenv.ErrInvalidCursor) {
		t.Fatalf("Unexpected success in setting the new value: %v", err)
	}

	if !bytes.Equal(block, []byte("\x00")) {
		t.Fatalf("Unexpected block after failed assignment: %q", string(block))
	}
}

func TestCursorDelete(t *testing.T) {
	block := []byte("key=foo\x00after=pristine\x00\x00\x00\x00\x00")
	cur := ubootenv.NewCursor(block)

	if cur.Next() != true {
		t.Fatalf("Expected to find the first entry")
	}

	if ent := cur.Entry(); ent != "key=foo\x00" {
		t.Fatalf("Unexpected entry before deletion: %q", ent)
	}

	cur.Delete()

	if ent := cur.Entry(); ent != "" {
		t.Fatalf("Unexpected entry after deletion: %q", ent)
	}

	if cur.Next() != true {
		t.Fatalf("Expected to find the second entry")
	}

	if ent := cur.Entry(); ent != "after=pristine\x00" {
		t.Fatalf("Unexpected entry deletion: %q", ent)
	}

	if !bytes.Equal(block, []byte("after=pristine\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00")) {
		t.Fatalf("Unexpected block after deletion: %q", string(block))
	}

	if cur.Next() != false {
		t.Fatalf("Expected to find only one entry")
	}
}

func TestDeleteSaturated(t *testing.T) {
	block := []byte("k=v\x00key=value\x00")
	cur := ubootenv.NewCursor(block)

	if cur.Next() != true {
		t.Fatalf("Expected to find the first entry")
	}

	if ent := cur.Entry(); ent != "k=v\x00" {
		t.Fatalf("Unexpected entry before deletion: %q", ent)
	}

	cur.Delete()

	if ent := cur.Entry(); ent != "" {
		t.Fatalf("Unexpected entry after deletion: %q", ent)
	}

	if cur.Next() != true {
		t.Fatalf("Expected to find the second entry")
	}

	if ent := cur.Entry(); ent != "key=value\x00" {
		t.Fatalf("Unexpected entry after deletion: %q", ent)
	}

	if !bytes.Equal(block, []byte("key=value\x00\x00\x00\x00\x00")) {
		t.Fatalf("Unexpected block after deletion: %q", string(block))
	}

	if cur.Next() != false {
		t.Fatalf("Expected to find only one entry")
	}
}

func TestInsertBeforeAtFront(t *testing.T) {
	block := []byte("a=1\x00\x00\x00\x00\x00")
	cur := ubootenv.NewCursor(block)

	if err := cur.InsertBefore("b", "2"); err != nil {
		t.Fatalf("Cannot insert entry: %v", err)
	}

	if ent := cur.Entry(); ent != "b=2\x00" {
		t.Fatalf("Unexpected entry after insertion: %q", ent)
	}

	if !bytes.Equal(block, []byte("b=2\x00a=1\x00")) {
		t.Fatalf("Unexpected block after insertion: %q", string(block))
	}

	if cur.Next() != true {
		t.Fatalf("Expected to find the second entry")
	}

	if ent := cur.Entry(); ent != "a=1\x00" {
		t.Fatalf("Unexpected second entry after insertion: %q", ent)
	}

	if cur.Next() != false {
		t.Fatalf("Expected to find only two entries")
	}
}

func TestInsertBeforeNotAtEnd(t *testing.T) {
	block := []byte("a=1\x00\x00\x00\x00\x00")
	cur := ubootenv.NewCursor(block)

	if cur.Next() != true {
		t.Fatalf("Expected to find one entry")
	}

	if ent := cur.Entry(); ent != "a=1\x00" {
		t.Fatalf("Unexpected entry before insertion: %q", ent)
	}

	if err := cur.InsertBefore("b", "2"); err != nil {
		t.Fatalf("Cannot insert entry: %v", err)
	}

	if ent := cur.Entry(); ent != "b=2\x00" {
		t.Fatalf("Unexpected entry after insertion: %q", ent)
	}

	if !bytes.Equal(block, []byte("b=2\x00a=1\x00")) {
		t.Fatalf("Unexpected block after insertion: %q", string(block))
	}

	if cur.Next() != true {
		t.Fatalf("Expected to find the second entry")
	}
}

func TestInsertBeforeNotAtEndInsufficientSpace(t *testing.T) {
	block := []byte("a=1000\x00\x00")
	cur := ubootenv.NewCursor(block)

	if cur.Next() != true {
		t.Fatalf("Expected to find one entry")
	}

	if ent := cur.Entry(); ent != "a=1000\x00" {
		t.Fatalf("Unexpected entry before insertion: %q", ent)
	}

	if err := cur.InsertBefore("b", "20"); !errors.Is(err, ubootenv.ErrInsufficientSpace) {
		t.Fatalf("Unexpected success to insert entry: %v", err)
	}

	if ent := cur.Entry(); ent != "a=1000\x00" {
		t.Fatalf("Unexpected entry after failed insertion: %q", ent)
	}

	if !bytes.Equal(block, []byte("a=1000\x00\x00")) {
		t.Fatalf("Unexpected block after failed insertion: %q", string(block))
	}

	if cur.Next() != false {
		t.Fatalf("Expected to find only one entry")
	}
}

func TestInsertBeforeAtEnd(t *testing.T) {
	block := []byte("\x00\x00\x00\x00")
	cur := ubootenv.NewCursor(block)

	if cur.Next() != false {
		t.Fatalf("Expected to find no entries")
	}

	if err := cur.InsertBefore("k", "v"); err != nil {
		t.Fatalf("Cannot insert entry: %v", err)
	}

	if ent := cur.Entry(); ent != "k=v\x00" {
		t.Fatalf("Unexpected entry after insertion: %q", ent)
	}

	if !bytes.Equal(block, []byte("k=v\x00")) {
		t.Fatalf("Unexpected block after insertion: %q", string(block))
	}

	if cur.Next() != false {
		t.Fatalf("Expected to find only one entry")
	}
}

func TestInsertBeforeAtEndInsufficientUnusedSpace(t *testing.T) {
	block := []byte("\x00\x00\x00")
	cur := ubootenv.NewCursor(block)

	if cur.Next() != false {
		t.Fatalf("Expected to find no entries")
	}

	if err := cur.InsertBefore("k", "v"); !errors.Is(err, ubootenv.ErrInsufficientSpace) {
		t.Fatalf("Unexpected success in inserting entry: %v", err)
	}

	if !bytes.Equal(block, []byte("\x00\x00\x00")) {
		t.Fatalf("Unexpected block after failed insertion: %q", string(block))
	}

	if cur.Next() != false {
		t.Fatalf("Expected to find no entries")
	}
}

func TestInsertBeforeFillingEmptyBuffer(t *testing.T) {
	block := []byte("\x00\x00\x00\x00")
	cur := ubootenv.NewCursor(block)

	if cur.Next() != false {
		t.Fatalf("Expected to find no entries")
	}

	if err := cur.InsertBefore("k", "v"); err != nil {
		t.Fatalf("Unexpected failure in inserting entry: %v", err)
	}

	if !bytes.Equal(block, []byte("k=v\x00")) {
		t.Fatalf("Unexpected block after failed insertion: %q", string(block))
	}

	if cur.Next() != false {
		t.Fatalf("Expected to find no entries")
	}
}

func TestInsertBeforeFillingRemainigBuffer(t *testing.T) {
	block := []byte("K=V\x00\x00\x00\x00\x00")
	cur := ubootenv.NewCursor(block)

	if cur.Next() != true {
		t.Fatalf("Expected to find the first entry")
	}

	if err := cur.InsertBefore("k", "v"); err != nil {
		t.Fatalf("Unexpected failure in inserting entry: %v", err)
	}

	if !bytes.Equal(block, []byte("k=v\x00K=V\x00")) {
		t.Fatalf("Unexpected block after failed insertion: %q", string(block))
	}

	if cur.Next() != true {
		t.Fatalf("Expected to find the initial entry")
	}
}

func TestDeleteValue(t *testing.T) {
	block := []byte("k=v\x00")
	cur := ubootenv.NewCursor(block)

	if cur.Next() != true {
		t.Fatalf("Expected to find the first entry")
	}

	cur.Delete()

	if !bytes.Equal(block, []byte("\x00\x00\x00\x00")) {
		t.Fatalf("Unexpected block after failed insertion: %q", string(block))
	}

	if cur.Next() != false {
		t.Fatalf("Expected not to find any entries")
	}
}

func TestInteractionsAtInvalidCursor(t *testing.T) {
	block := make([]byte, 32)
	cur := ubootenv.NewCursor(block)

	if cur.Next() != false {
		t.Fatalf("Expected not to find any entires")
	}

	if k := cur.Key(); k != "" {
		t.Fatalf("Unexpected key: %q", k)
	}

	if v := cur.Value(); v != "" {
		t.Fatalf("Unexpected value: %q", v)
	}

	if e := cur.Entry(); e != "" {
		t.Fatalf("Unexpected entry: %q", e)
	}

	if err := cur.SetValue("v"); err != ubootenv.ErrInvalidCursor {
		t.Fatalf("Unexpected error: %v", err)
	}
}

func TestInteractionsWithZeroSizedEnv(t *testing.T) {
	cur := ubootenv.NewCursor(nil)

	if cur.Next() != false {
		t.Fatalf("Expected not to find any entires")
	}

	if k := cur.Key(); k != "" {
		t.Fatalf("Unexpected key: %q", k)
	}

	if v := cur.Value(); v != "" {
		t.Fatalf("Unexpected value: %q", v)
	}

	if e := cur.Entry(); e != "" {
		t.Fatalf("Unexpected entry: %q", e)
	}

	if err := cur.SetValue("v"); err != ubootenv.ErrInvalidCursor {
		t.Fatalf("Unexpected error: %v", err)
	}
}
